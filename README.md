# 3. CKA Pratice Exam Part 3 ACG

------------

><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______


# Objectifs

1. Créer un compte de service

2. Créer un ClusterRole qui fournit un accès en lecture aux pods

3. Liez le ClusterRole au compte de service pour lire uniquement les pods dans l'espace de noms Web.


# Contexte

Cet atelier propose des scénarios pratiques pour vous aider à vous préparer à l'examen Certified Kubernetes Administrator (CKA). Des tâches à accomplir vous seront présentées ainsi que des serveurs et/ou un cluster Kubernetes existant pour les accomplir. Vous devrez utiliser vos connaissances de Kubernetes pour mener à bien les tâches fournies, un peu comme vous le feriez sur le terrain réel. Examen CKA. Bonne chance!

>![Alt text](img/image.png)

# Introduction
Ce laboratoire fournit des scénarios de pratique pour vous aider à vous préparer à l'examen Certified Kubernetes Administrator (CKA). Vous serez présenté avec des tâches à accomplir ainsi que des serveurs et/ou un cluster Kubernetes existant pour les compléter. Vous devrez utiliser vos connaissances de Kubernetes pour réussir les tâches fournies, tout comme vous le feriez lors de l'examen CKA réel. Bonne chance !

# Application

## Étape 1 : Connexion au Serveur

Connectez-vous au serveur à l'aide des informations d'identification fournies :

```sh
ssh cloud_user@<PUBLIC_IP_ADDRESS>
```

## Étape 2 : Créer un Compte de Service

1. Passez au contexte approprié avec kubectl :

```sh
kubectl config use-context acgk8s
```

2. Créez un compte de service :

```sh
kubectl create sa webautomation -n web
```

3. Vérification 

```sh
kubectl get sa -n web
```

>![Alt text](img/image-3.png)

## Étape 3 : Créer un ClusterRole Qui Fournit un Accès en Lecture aux Pods

1. Créez un fichier `pod-reader.yml` :

```sh
kubectl create clusterrole pod-reader --verb=get,list,watch --resource=pods --dry-run=client -o yaml > pod-reader.yml
```

```sh
vi pod-reader.yml
```

2. Définissez le ClusterRole :

```yaml
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
    name: pod-reader
rules:
- apiGroups: [""]
    resources: ["pods"]
    verbs: ["get", "watch", "list"]
```
ou

```yaml
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  name: pod-reader
rules:
- apiGroups:
  - ""
  resources:
  - pods
  verbs:
  - get
  - list
  - watch
```

3. Appuyez sur `Esc` et entrez `:wq` pour enregistrer et quitter.

4. Créez le ClusterRole :

```sh
kubectl create -f pod-reader.yml
```


5. Vérifier les clusterrole

```sh
kubectl get clusterrole
```

>![Alt text](img/image-1.png)
*Liste des cluster role*

6. Description du clusterrole

```sh
kubectl describe clusterrole pod-reader
```

>![Alt text](img/image-4.png)
*Description du cluster role*

## Étape 4 : Lier le ClusterRole au Compte de Service pour Lire Uniquement les Pods dans le Namespace `web`

1. Créez le fichier `rb-pod-reader.yml` :

```sh
vi rb-pod-reader.yml
```

2. Définissez le RoleBinding :

```yaml
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  name: rb-pod-reader
  namespace: web
subjects:
- kind: ServiceAccount
  name: webautomation # "name" is case sensitive
  apiGroup: rbac.authorization.k8s.io
roleRef:
  kind: ClusterRole #this must be Role or ClusterRole
  name: pod-reader # this must match the name of the Role or ClusterRole you wish to bind to
  apiGroup: rbac.authorization.k8s.io
```

Ref : https://kubernetes.io/docs/reference/access-authn-authz/rbac/#rolebinding-example


3. Appuyez sur `Esc` et entrez `:wq` pour enregistrer et quitter.

4. Créez le RoleBinding :

```sh
kubectl create -f rb-pod-reader.yml
```

>![Alt text](img/image-2.png)

5. Vérification du role

```sh
kubectl get rolebinding -n web
```

>![Alt text](img/image-5.png)

## Étape 5 : Vérifier que le RoleBinding Fonctionne

1. Vérifiez que le RoleBinding fonctionne :

```sh
kubectl get pods -n web --as=system:serviceaccount:web:webautomation
```
>![Alt text](img/image-6.png)

Nous avons bel et bien accès en lecture aux ressources du namespace web

En suivant ces étapes, vous aurez créé un compte de service, un ClusterRole avec accès en lecture aux pods, et lié ce ClusterRole au compte de service dans le namespace `web`. Vous aurez également vérifié que le compte de service a les permissions appropriées.